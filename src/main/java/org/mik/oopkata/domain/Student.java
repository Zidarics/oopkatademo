package org.mik.oopkata.domain;

public class Student extends AbstractPerson {
//RBEGIN
	private Specialization specialization;
	
	public Student(String name, int birthYear, Specialization specialization) {
		super(name, birthYear);
		this.specialization = specialization;
	}

	/**
	 * @return the specialization
	 */
	@Override
	public Specialization getSpecialization() {
		return this.specialization;
	}

	/**
	 * @param specialization the specialization to set
	 */
	public void setSpecialization(Specialization specialization) {
		this.specialization = specialization;
	}

	@Override
	public boolean isStudent() {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.specialization == null) ? 0 : this.specialization.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (this.specialization != other.specialization)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return new StringBuilder(super.toString()).append(' ').append(this.specialization).toString();
	}//REND
}
