package org.mik.oopkata.domain;

public enum Specialization {

	INFORMATICS, MECHANICS, ELECTRIC, ARCHITECT; 
}
