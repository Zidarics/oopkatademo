package org.mik.oopkata.domain;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class University {
//RBEGIN
	private Set<Student> students = new HashSet<>();
	
	private Set<Teacher> teachers = new HashSet<>();
	
	private Set<Assistant> assistants = new HashSet<>();
			
	
	public University() {
	}
	
	public boolean addStudent(Student st) {
		if (this.students.contains(st))
			return false;
		
		this.students.add(st);
		return true;
	}
	
	public void removeStudent(Student st) {
		this.students.remove(st);
	}
	
	public boolean addTeacher(Teacher t) {
		if (this.teachers.contains(t))
			return false;
		
		this.teachers.add(t);
		return true;
	}
	
	public void removeTeachert(Teacher t) {
		this.teachers.remove(t);
	}
	
	public boolean addAssistant(Assistant a) {
		if (this.assistants.contains(a))
			return false;
		
		this.assistants.add(a);
		return true;
	}
	
	public void removeAssistant(Assistant a) {
		this.assistants.remove(a);
	}

	/**
	 * @return the students
	 */
	public Set<Student> getStudents() {
		return Collections.unmodifiableSet(this.students);
	}

	/**
	 * @return the teachers
	 */
	public Set<Teacher> getTeachers() {
		return Collections.unmodifiableSet(this.teachers);
	}

	/**
	 * @return the assistants
	 */
	public Set<Assistant> getAssistants() {
		return Collections.unmodifiableSet(this.assistants);
	}
	
	
	public Person findPersonByName(String name) {
		Person p = findPersonByName(this.assistants, name);
		if (p!=null)
			return p;

		p = findPersonByName(this.students, name);
		if (p!=null)
			return p;
		
		p = findPersonByName(this.teachers, name);
		if (p!=null)
			return p;

		return null;
	}

	private static Person findPersonByName(Set<? extends Person> set, String name) {
		for(Person p:set)
			if (p.getName().compareTo(name)==0)
				return p;
		
		return null;
	}
	
	public Set<Person> findPersonByBirthYear(int year) {
		Set<Person> result = new HashSet<>();
		findPersonByBirthYear(this.assistants, result, year);
		findPersonByBirthYear(this.students, result, year);
		findPersonByBirthYear(this.teachers, result, year);
		return result;
	}

	private static void findPersonByBirthYear(Set<? extends Person> find, Set<Person> result, int year) {
		for(Person p:find)
			if (p.getBirthYear()==year)
				result.add(p);
	}//REND

}
