package org.mik.oopkata.domain;

public interface Person {
//RBEGIN
	String getName();
	
	int getBirthYear();
	
	boolean isStudent();
	
	boolean isTeacher();
	
	boolean isAssistant();
	
	Institute getInstitute() throws Exception;
	
	Specialization getSpecialization() throws Exception;
//REND
}
