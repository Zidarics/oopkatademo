package org.mik.oopkata.domain;

public class Teacher extends AbstractPerson {
//RBEGIN
	private Institute institute;
	
	
	public Teacher(String name, int birthYear, Institute institute) {
		super(name, birthYear);
		this.institute = institute;
	}


	/**
	 * @return the institute
	 */
	@Override
	public Institute getInstitute() {
		return this.institute;
	}


	/**
	 * @param institute the institute to set
	 */
	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	@Override
	public boolean isTeacher() {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.institute == null) ? 0 : this.institute.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Teacher other = (Teacher) obj;
		if (this.institute != other.institute)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return new StringBuilder(super.toString()).append(' ').append(this.institute).toString();
	}//REND

}
