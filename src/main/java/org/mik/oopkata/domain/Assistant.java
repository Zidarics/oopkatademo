package org.mik.oopkata.domain;

public class Assistant extends AbstractPerson {
//RBEGIN
	public Assistant(String name, int birthYear) {
		super(name, birthYear);
	}

	@Override
	public boolean isAssistant() {
		return true;
	}//REND
	
}
