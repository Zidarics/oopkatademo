package org.mik.oopkata.domain;

public abstract class AbstractPerson implements Person {

	public static final int MIN_NAME_LENGTH = 3;
	
	public static final int MIN_YEAR = 1929;
	
	public static final int MAX_YEAR = 2000;
//RBEGIN	
	private String name;
	
	private int birthYear;

	public AbstractPerson(String name, int birthYear) {
		this.name = name;
		this.birthYear = birthYear;
	}

	/**
	 * @return the birthYear
	 */
	@Override
	public int getBirthYear() {
		return this.birthYear;
	}

	/**
	 * @param birthYear the birthYear to set
	 */
	public void setBirthYear(int birthYear) {
		if (birthYear>=MIN_YEAR && birthYear <=MAX_YEAR)
			this.birthYear = birthYear;
	}

	/**
	 * @return the name
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		if (name!=null && name.length()>=MIN_NAME_LENGTH)
			this.name = name;
	}

	@Override
	public boolean isAssistant() {
		return false;
	}
	
	@Override
	public boolean isTeacher() {
		return false;
	}
	
	@Override
	public boolean isStudent() {
		return false;
	}
	
	@Override
	public Institute getInstitute() throws Exception {
		throw new Exception(String.format("%s does not have institute", getClass().getName())); //$NON-NLS-1$
	}
	
	@Override
	public Specialization getSpecialization() throws Exception {
		throw new Exception(String.format("%s does not have specialization", getClass().getName())); //$NON-NLS-1$
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.birthYear;
		result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractPerson other = (AbstractPerson) obj;
		if (this.birthYear != other.birthYear)
			return false;
		if (this.name == null) {
			if (other.name != null)
				return false;
		} else if (!this.name.equals(other.name))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return new StringBuilder(this.name).append(' ').append(this.birthYear).toString();
	}//REND
}
