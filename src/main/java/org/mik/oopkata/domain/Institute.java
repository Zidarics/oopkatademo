package org.mik.oopkata.domain;

public enum Institute {

	MECHANICS, INFORMATICS, ELECTRICAL, ARCHITECT;
}
