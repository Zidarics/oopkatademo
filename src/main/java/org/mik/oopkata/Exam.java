package org.mik.oopkata;

/**
 * OOP Kata exam
 *
 */
public class Exam
{

    public static String getName() {//RBEGIN
	return "Zidarics Zoltan"; //$NON-NLS-1$ //REND
    }
    
    public static String getNeptunCode() { //RBEGIN
	return "zizpfabp.pte"; //$NON-NLS-1$ //REND
    }

    public static void main( String[] args )
    {
        System.out.println( "Hello World!" ); //$NON-NLS-1$
    }
}
